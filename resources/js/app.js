require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import swal from 'sweetalert2';
import App from './components/App.vue';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

window.swal = swal;

import AbsensiIndex from './components/Absensi.vue';
import AbsensiAdd from './components/AbsensiAdd.vue';
import AbsensiEdit from './components/AbsensiEdit.vue';

const routes = [
    {
        name: 'home',
        path: '/',
        component: AbsensiIndex
    },
    {
        name: 'tambah',
        path: '/absensi/tambah',
        component: AbsensiAdd
    },
    {
        name: 'edit',
        path: '/absensi/edit',
        component: AbsensiEdit
    },
];
   
const router = new VueRouter({ mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');