<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Mahasiswa;
use Faker\Generator as Faker;

$factory->define(Mahasiswa::class, function (Faker $faker) {
    return [
        'nama' => $this->faker->name,
        'nim' => rand(),
        'gender' => 'L',
        'fakultas' => 'Ilmu Komputer',
        'prodi' => 'Teknik Informatika',
        'alamat' => $faker->address,
    ];
});
