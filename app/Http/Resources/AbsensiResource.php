<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AbsensiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->mahasiswa->nama,
            'nim' => $this->mahasiswa->nim,
            'mahasiswa_id' => $this->mahasiswa_id,
            'matkul' => $this->matkul,
            'kehadiran' => $this->kehadiran
        ];
    }
}
