<?php

namespace App\Http\Controllers;

use App\Absensi;
use App\Http\Resources\AbsensiResource;
use App\Mahasiswa;
use Illuminate\Http\Request;

class AbsensiController extends Controller
{
    public function all() {
        $absensi = AbsensiResource::collection(Absensi::all());
        return response()->json($absensi);
    }

    public function store(Request $request) {
        $mhs = Absensi::where('mahasiswa_id', $request->mahasiswa_id)->first();
        if(is_null($mhs)) {
            $absensi = Absensi::create([
                'mahasiswa_id' => $request->mahasiswa_id,
                'matkul' => $request->matkul,
                'kehadiran' => $request->kehadiran
            ]);

            $abs_mhs = Mahasiswa::find($request->mahasiswa_id);
            $abs_mhs->absensi_id = $absensi->id;
            $abs_mhs->save();

            if($absensi) {
                return response()->json(['status' => 'success', 'data' => $absensi]);
            }
        } 
        return response('failed', 500);
    }

    public function update(Request $request) {
        $mhs = Absensi::where('mahasiswa_id', $request->mahasiswa_id)->first();
        if(is_null($mhs)) {
            $absensi = Absensi::find($request->id);
            $absensi->mahasiswa_id = $request->mahasiswa_id;
            $absensi->matkul = $request->matkul;
            $absensi->kehadiran = $request->kehadiran;
            $absensi->save();
            if($absensi) {
                return response()->json(['status' => 'success', 'data' => $absensi]);
            }
        }
        return response('failed', 500);
    }

    public function destroy($id) {
        $absensi = Absensi::find($id);
        $absensi->delete();
    }
}
