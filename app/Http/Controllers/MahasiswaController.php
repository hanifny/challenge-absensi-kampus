<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function all() {
        $mhs = Mahasiswa::all();
        return response()->json($mhs);
    }
}
