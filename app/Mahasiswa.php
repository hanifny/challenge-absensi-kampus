<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';
    public function absensi() {
        return $this->hasOne(Absensi::class, 'mahasiswa_id');
    }
}
