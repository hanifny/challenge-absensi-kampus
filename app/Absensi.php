<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $table = 'absensi';
    protected $fillable = ['kehadiran', 'mahasiswa_id', 'matkul'];

    public function mahasiswa() {
        return $this->belongsTo(Mahasiswa::class);
    }
}
