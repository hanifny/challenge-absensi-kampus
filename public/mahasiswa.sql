-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 22, 2020 at 02:00 AM
-- Server version: 5.7.31-0ubuntu0.18.04.1
-- PHP Version: 7.2.33-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `challenges`
--

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `absensi_id` bigint(20) UNSIGNED DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('L','P') COLLATE utf8mb4_unicode_ci NOT NULL,
  `fakultas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prodi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nim`, `absensi_id`, `nama`, `gender`, `fakultas`, `prodi`, `alamat`, `created_at`, `updated_at`) VALUES
(1, '751602604', NULL, 'Colton Quitzon', 'L', 'Ilmu Komputer', 'Teknik Informatika', '571 Camron Islands\nOrnside, KS 25038', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(2, '1681336777', 3, 'Martine Farrell DDS', 'L', 'Ilmu Komputer', 'Teknik Informatika', '746 Aubrey Lights\nGrimesport, ND 15452', '2020-11-21 11:58:03', '2020-11-21 11:59:11'),
(3, '855494223', NULL, 'Halie Spencer', 'L', 'Ilmu Komputer', 'Teknik Informatika', '144 Hirthe Cove\nNorth Faystad, OH 32240-7974', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(4, '269224448', 1, 'Christ Ritchie', 'L', 'Ilmu Komputer', 'Teknik Informatika', '604 Feeney Burg\nEast Tressieberg, MN 58314', '2020-11-21 11:58:03', '2020-11-21 11:58:24'),
(5, '1888916398', NULL, 'Megane Jaskolski', 'L', 'Ilmu Komputer', 'Teknik Informatika', '568 Laurie Bridge Apt. 869\nNew Markusshire, WI 15588', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(6, '1545383376', NULL, 'Malika Hand', 'L', 'Ilmu Komputer', 'Teknik Informatika', '66096 Dietrich Stream Apt. 963\nNew Lambertside, PA 65932-5831', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(7, '429025866', NULL, 'Francisca Bogisich', 'L', 'Ilmu Komputer', 'Teknik Informatika', '6677 Margaret Ferry\nAshtonshire, MS 87585-7296', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(8, '1715468037', NULL, 'Mrs. Amira Lesch DDS', 'L', 'Ilmu Komputer', 'Teknik Informatika', '4039 Shyanne Trail Suite 313\nAbbeyburgh, NH 70939', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(9, '1937769408', NULL, 'Jennifer Doyle', 'L', 'Ilmu Komputer', 'Teknik Informatika', '1900 Waelchi Valley Apt. 720\nConroyfurt, LA 14085-8529', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(10, '1788177401', NULL, 'Forrest Wyman', 'L', 'Ilmu Komputer', 'Teknik Informatika', '796 Terrill Ports\nSouth Colten, SC 54725-2863', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(11, '716352404', NULL, 'Christine Rodriguez', 'L', 'Ilmu Komputer', 'Teknik Informatika', '9263 Alisha Grove Apt. 766\nEffertzview, WA 31125', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(12, '1389741611', 4, 'Amari Howell', 'L', 'Ilmu Komputer', 'Teknik Informatika', '9879 Little Station\nJusticebury, MA 62102-3386', '2020-11-21 11:58:03', '2020-11-21 11:59:19'),
(13, '1820893417', 5, 'Arlie Langosh III', 'L', 'Ilmu Komputer', 'Teknik Informatika', '997 Lebsack Terrace Suite 984\nKihnchester, MA 02754', '2020-11-21 11:58:03', '2020-11-21 11:59:30'),
(14, '1485495060', 2, 'Veda Schaefer MD', 'L', 'Ilmu Komputer', 'Teknik Informatika', '334 Karlee Parkways Suite 766\nSmithamside, NM 78736', '2020-11-21 11:58:03', '2020-11-21 11:58:31'),
(15, '1509499619', NULL, 'Enrico Flatley', 'L', 'Ilmu Komputer', 'Teknik Informatika', '542 Blanda Flat Apt. 747\nNew Fabianbury, AL 76169-7085', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(16, '179955098', NULL, 'Kathlyn Hoppe', 'L', 'Ilmu Komputer', 'Teknik Informatika', '493 Paucek Fork\nAlyciaside, GA 89743', '2020-11-21 11:58:03', '2020-11-21 11:58:03'),
(17, '959043119', NULL, 'Dr. Hermann Weissnat', 'L', 'Ilmu Komputer', 'Teknik Informatika', '98629 Celine Summit\nBorerfurt, CT 13768', '2020-11-21 11:58:04', '2020-11-21 11:58:04'),
(18, '625986204', NULL, 'Astrid Kutch', 'L', 'Ilmu Komputer', 'Teknik Informatika', '615 Nikolaus Crest Apt. 810\nMarvinbury, OH 88516', '2020-11-21 11:58:04', '2020-11-21 11:58:04'),
(19, '1455329297', NULL, 'Prof. Rosetta West Sr.', 'L', 'Ilmu Komputer', 'Teknik Informatika', '296 Swaniawski Walk\nSouth Jayme, CT 75214', '2020-11-21 11:58:04', '2020-11-21 11:58:04'),
(20, '1873268041', NULL, 'Ms. Roxanne Gaylord', 'L', 'Ilmu Komputer', 'Teknik Informatika', '104 Jacobson Viaduct\nAbelardoview, ND 27531-3566', '2020-11-21 11:58:04', '2020-11-21 11:58:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mahasiswa_nim_unique` (`nim`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
