<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Absensi
Route::get('/absensi', 'AbsensiController@all');
Route::post('/absensi', 'AbsensiController@store');
Route::put('/absensi/{id}', 'AbsensiController@update');
Route::delete('/absensi/{id}', 'AbsensiController@destroy');

// Mahasiswa
Route::get('/mahasiswa', 'MahasiswaController@all');